//
//  StoreLayer.m
//  sugarPuzzle
//
//  Created by takumi on 2013/01/01.
//  Copyright (c) 2013年 takumi ninomiya. All rights reserved.
//

#import <Parse/Parse.h>
#import "StoreLayer.h"
#import "Config.h"
#import "CCScrollLayer.h"



@implementation StoreLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	StoreLayer *layer = [StoreLayer node];
    
	[scene addChild: layer];
	
	return scene;
}

-(id) init
{
	if( (self=[super init])) {
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *background;
		
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
			background = [CCSprite spriteWithFile:@"images/backgroundTile.png"];
		} else {
			background = [CCSprite spriteWithFile:@"images/backgroundTile-Landscape.png"];
		}
		background.position = ccp(size.width/2, size.height/2);
        [self addChild:background];
        
        [self setPuroducts];
        
        //タッチアクションを有効化する
        [self setTouchEnabled:YES];
        
	}
	return self;
}


-(void)setPuroducts{
    
    CCMenuItem *item = [CCMenuItemImage itemWithNormalImage:@"images/image_01.png" selectedImage:@"images/image_01.png" target:self selector:@selector(byeProduct:)];
    CCMenu *menu = [CCMenu menuWithItems : item , nil];
    [self addChild:menu];
    
    
    //NSLog(@"query is %@", productQuery);
    
    /*
    NSMutableArray *layerArray = [NSMutableArray array];
    
    for(int i = 0; i < 4; i++){
        CCLayer *block = [CCLayer node];
        block.tag = i;
        [layerArray addObject:block];
    }
    
    CCScrollLayer *scrolllayer = [[CCScrollLayer alloc] initWithLayers:layerArray widthOffset:0];
    scrolllayer.showPagesIndicator = NO;
    [self addChild:scrolllayer];
     */
}

-(void)byeProduct:(CCMenuItem *)item{
    CCLOG(@"loglog");
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(
                                                         NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSLog(@"%@", paths[0]);
    
    
    [Parse setApplicationId:@"RCKjYcS5Uy3eGI1eYDnl9z1ixYhf1XeX99rbkrFW"
                  clientKey:@"KoNjdZdjZyMYSx0F72611rzEs6bwioy1WoHsHWNu"];
    
    // Use the product identifier from iTunes to register a handler.
    [PFPurchase addObserverForProduct:@"com.csreporters.sugerPuzzle.appPack" block:^(SKPaymentTransaction *transaction) {
        // Write business logic that should run once this product is purchased.
        //isPro = YES;
        CCLOG(@"loglog");
    }];
    
    /*
    [PFPurchase addObserverForProduct:@"3jOJlCWa8Q" block:^(SKPaymentTransaction *transaction) {
        [PFPurchase downloadAssetForTransaction:transaction completion:^(NSString *filePath, NSError *error) {
            if (!error) {
                
                // at this point, the content file is available at filePath.
            }else{
                CCLOG(@"loglog2");
            }
        }];
    }];
     */
    
    
}

-(void) registerWithTouchDispatcher
{
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
}
-(void) ccTouchCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
}

@end
