//
//  NewsLayer.m
//  sugarPuzzle
//
//  Created by takumi on 2013/01/09.
//  Copyright (c) 2013年 takumi ninomiya. All rights reserved.
//

#import "NewsLayer.h"
#import "MenuLayer.h"
#import "Reachability.h"

@implementation NewsLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	NewsLayer *layer = [NewsLayer node];
    
	[scene addChild: layer];
	
	return scene;
}


-(id) init
{
	if( (self=[super init])) {
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *background;
		
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
			background = [CCSprite spriteWithFile:@"images/backgroundTile.png"];
		} else {
			background = [CCSprite spriteWithFile:@"images/backgroundTile-Landscape.png"];
		}
		background.position = ccp(size.width/2, size.height/2);
        [self addChild:background];
        
        
        CCSprite *board = [CCSprite spriteWithFile:@"images/newsBG.png"];
        board.position = ccp(size.width / 2, size.height / 2);
        [self addChild:board];
        
        CCMenuItemImage *close = [CCMenuItemImage itemWithNormalImage:@"images/newsClose.png" selectedImage:@"images/newsClose.png" target:self selector:@selector(pushClose:)];
        CCMenu *button = [CCMenu menuWithItems:close, nil];
        
        button.position = ccp((size.width / 2) + 200, (size.height / 2) + 120);
        [board addChild:button];
        
        [self netWorkCheck];
        
        view = [[UIView alloc] initWithFrame:CGRectMake((size.width - (size.width * 0.7)) / 4 ,(size.height  - (size.height * 0.8)) / 4 ,size.width * 0.7 , size.height * 0.8)];
        
        UIWebView *web = [[UIWebView alloc]initWithFrame:view.frame];
        
        NSURL *url = [NSURL URLWithString:@"http://web-cp.com/medeve/"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        //読み込み開始
        [web loadRequest:request];
        //ここもお好みで(画面サイズにページを合わせるか)
        web.scalesPageToFit = YES;
        
        //viewの上にwebviewを乗っける
        [view addSubview:web];
        
        //cocos2dの上に乗っける
        [[[CCDirector sharedDirector] view] addSubview:view];
        
        
        //タッチアクションを有効化する
        [self setTouchEnabled:YES];
        
	}
	return self;
}

-(BOOL) netWorkCheck{
    Reachability *hostReach = [Reachability reachabilityForInternetConnection];
    
    bool flag = true;
    
    switch ([hostReach currentReachabilityStatus]) {
        case NotReachable:

            
            flag = false;
            
            //alert
            UIAlertView *alert = [[UIAlertView alloc] init];
            alert.delegate = self;
            alert.title = @"ネットに繋がりません";
            alert.message = [NSString stringWithFormat:@"インターネットに繋がる場所でご利用ください"];
            [alert addButtonWithTitle:@"はい"];
            [alert show];
            
            break;
            
            case ReachableViaWiFi:
            break;
            
            case ReachableViaWWAN:
            break;
    }
    
    return flag;
    
    
}


//alertのボタンを押したときにリロード
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
}


-(void) pushClose:(CCMenuItem *)menuItem{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"NO" forKey:@"isFirst"];
    BOOL successful = [defaults synchronize];
    if (successful) {
        NSLog(@"%@", @"データの保存に成功しました。");
    }
    
    for (view in [view subviews]) {
        [view removeFromSuperview];
    }
    [[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:0.5 scene:[MenuLayer scene] ]];
    /*
    CCLOG(@"test");
    CGSize size = [[CCDirector sharedDirector] winSize];
    CCMoveBy *moveby = [CCMoveBy actionWithDuration:0.5 position:ccp(0, -size.height)];
    
    [self runAction:moveby];
    
    //アニメーションの対象となるコンテキスト
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
    //アニメーションを実行する時間
    [UIView setAnimationDuration:0.5];
    //アニメーションイベントを受け取るview
    [UIView setAnimationDelegate:self];
    //アニメーション終了後に実行される
    [UIView setAnimationDidStopSelector:@selector(endAnimation)];
    
    //座標を変更する1
    CGRect rect = [view frame];
    rect.origin.y = size.height + rect.origin.y;
    [view setFrame:rect];
    
    // アニメーション開始
    [UIView commitAnimations];
     */
     
}

-(void) registerWithTouchDispatcher
{
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
}
-(void) ccTouchCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
}



@end
