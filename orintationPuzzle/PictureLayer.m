//
//  PictureLayer.m
//  sugarPuzzle
//
//  Created by takumi on 2013/01/15.
//  Copyright 2013年 takumi ninomiya. All rights reserved.
//

#import "Config.h"
#import "PictureLayer.h"
#import "GameLayer.h"
#import "CCScrollLayer.h"


@implementation PictureLayer

+(CCScene *) scene
{
    CCScene *scene = [CCScene node];
	PictureLayer *layer = [PictureLayer node];
	[scene addChild: layer];
	return scene;
}

//
-(id) init
{
	if( (self=[super init])) {
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *background;
		
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
			background = [CCSprite spriteWithFile:@"images/backgroundTile.png"];
		} else {
			background = [CCSprite spriteWithFile:@"images/backgroundTile-Landscape.png"];
		}
		background.position = ccp(size.width/2, size.height/2);
        
        [self addChild: background];
        
        [self setPictures];
        
        //タッチアクションを有効化する
        [self setTouchEnabled:YES];
        
	}
	return self;
}


-(void)setPictures{
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    //int *_id;
    //_id = selectSeries;

    //int max = 5;
    
    NSString *dir = @"images/image_0";
    NSString *type = @".png";
    
    
    NSMutableArray *layerArray = [NSMutableArray array];
    for(int i = 0; i < 4; i++){
        NSString *file = [NSString stringWithFormat:@"%@%d%@",dir,(i + 1),type];
        
        CCLayer *block = [CCLayer node];
        CCSprite *picture = [CCSprite spriteWithFile:file];
        
        
        picture.position = ccp(size.width/2, size.height/2);
        picture.scale = 0.32f;
        picture.tag = i + 1;
        [block addChild:picture];
        
        [layerArray addObject:block];
    }
    
    CCScrollLayer *scrolllayer = [[CCScrollLayer alloc] initWithLayers:layerArray widthOffset:0];
    scrolllayer.showPagesIndicator = NO;
    [self addChild:scrolllayer];
    
    [self setLevels];

}

-(void) setLevels{
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    CCMenuItemImage *bt_easy = [CCMenuItemImage itemWithNormalImage:@"images/bt_easy.png" selectedImage:@"images/bt_easy.png" target:self selector:@selector(gameStart:)];
    bt_easy.tag = 3;
    bt_easy.position = ccp(size.width / 2 - 200, 50);
    
    CCMenuItemImage *bt_normal = [CCMenuItemImage itemWithNormalImage:@"images/bt_normal.png" selectedImage:@"images/bt_normal.png" target:self selector:@selector(gameStart:)];
    bt_normal.tag = 4;
    bt_normal.position = ccp(size.width / 2, 50);
    
    CCMenuItemImage *bt_hard = [CCMenuItemImage itemWithNormalImage:@"images/bt_hard.png" selectedImage:@"images/bt_hard.png" target:self selector:@selector(gameStart:)];
    bt_hard.tag = 5;
    bt_hard.position = ccp(size.width / 2 + 200, 50);
    
    CCMenu *menu = [CCMenu menuWithItems:bt_easy,bt_normal,bt_hard, nil];
    menu.position = ccp(0,0);
    [self addChild:menu];
    
}

-(void)gameStart:(CCMenuItem*)item{
    switch (item.tag) {
        case 3:
            levels = 3;
        break;
            
        case 4:
            levels = 4;
        break;
            
        case 5:
            levels = 5;
        break;
            
        default:
            break;
    }
    
    selectImagePath = @"images/image_01.png";
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameLayer scene] ]];
    
}


-(void) registerWithTouchDispatcher
{
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
}
-(void) ccTouchCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
}

-(void) onEnter
{
	[super onEnter];
	//[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[HelloWorldLayer scene] ]];
}

@end
