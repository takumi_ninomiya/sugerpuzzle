//
//  IntroScene.m
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/09.
//  Copyright takumi ninomiya 2012年. All rights reserved.
//


// Import the interfaces
#import "IntroLayer.h"
#import "TopLayer.h"


#pragma mark - IntroLayer

// HelloWorldLayer implementation
@implementation IntroLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	IntroLayer *layer = [IntroLayer node];
	[scene addChild: layer];
	return scene;
}

// 
-(id) init
{
	if( (self=[super init])) {
		CGSize size = [[CCDirector sharedDirector] winSize];

		CCSprite *background;
		
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
			background = [CCSprite spriteWithFile:@"images/backgroundTile.png"];
		} else {
			background = [CCSprite spriteWithFile:@"images/backgroundTile-Landscape.png"];
		}
		background.position = ccp(size.width/2, size.height/2);
		//[self addChild: background];
        [self deviceChecker];
        isAdBannerVisible = NO;
	}
	return self;
}

-(void)deviceChecker{
    if( [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone )    
    {
        //デバイスがiphoneだった場合
        CCLOG(@"device is iPhone");
    }else{
        //デバイスがipadだった場合
        CCLOG(@"device is iPad");
    }
}

-(void) onEnter
{
	[super onEnter];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[TopLayer scene] ]];
}
@end
