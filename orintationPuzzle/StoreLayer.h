//
//  StoreLayer.h
//  sugarPuzzle
//
//  Created by takumi on 2013/01/01.
//  Copyright (c) 2013年 takumi ninomiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Config.h"
#import "CCScrollLayer.h"

@interface StoreLayer : CCLayer{
    NSMutableArray *dict;
}

+(CCScene *)scene;

@end
