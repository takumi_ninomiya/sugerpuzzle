//
//  LibraryLayer.h
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/13.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import "Config.h"

@interface LibraryLayer : CCLayer<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    
    UIImage *newImage;
}

+(CCScene *)scene;

@end
