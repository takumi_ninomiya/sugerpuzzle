//
//  Timer.m
//  orientationPuzzle
//
//  Created by takumi ninomiya on 2012/11/22.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import "Timer.h"

@implementation Timer

-(id) init
{
    if((self=[super init])) {
        [self setInterval];
    
    }
    return self;
}


-(void)setInterval{
    NSDate *startTime = [NSDate date];
    
    NSTimeInterval elapsedTime = [startTime timeIntervalSinceNow];
    NSString *elapsedString = [NSString stringWithFormat:@"time: %f ,秒", -elapsedTime];
    
    timer = elapsedString;
    
    NSLog(@"%@", elapsedString);
}


@end
