//
//  ConfigLayer.m
//  orientationPuzzle
//
//  Created by takumi ninomiya on 2012/11/22.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import "ConfigLayer.h"

@implementation ConfigLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	ConfigLayer *layer = [ConfigLayer node];
    
	[scene addChild: layer];
	
	return scene;
}

//
-(id) init
{
	if( (self=[super init])) {
		// ask director for the window size
		//CGSize size = [[CCDirector sharedDirector] winSize];
        
        //タッチアクションを有効化する
        [self setTouchEnabled:YES];
        
	}
	return self;
}

-(void) registerWithTouchDispatcher
{
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
}
-(void) ccTouchCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
}


@end
