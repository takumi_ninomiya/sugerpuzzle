//
//  NewsLayer.h
//  sugarPuzzle
//
//  Created by takumi on 2013/01/09.
//  Copyright (c) 2013年 takumi ninomiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface NewsLayer :CCLayer<UIAlertViewDelegate,UITableViewDelegate,UITableViewDataSource>{
    
    UIView *view;
}

+ (CCScene *)scene;

@end
