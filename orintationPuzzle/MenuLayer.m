//
//  MenuLayer.m
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/09.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import <Parse/Parse.h>
#import "MenuLayer.h"
#import "SelectLayer.h"
#import "ConfigLayer.h"
#import "CreditLayer.h"
#import "StoreLayer.h"
#import "NewsLayer.h"


@implementation MenuLayer

+(CCScene *) scene
{
    CCScene *scene = [CCScene node];
	MenuLayer *layer = [MenuLayer node];
	[scene addChild: layer];
	return scene;
}

//
-(id) init
{
	if( (self=[super init])) {
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
		CCSprite *background;
		
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
			background = [CCSprite spriteWithFile:@"images/backgroundTile.png"];
		} else {
			background = [CCSprite spriteWithFile:@"images/backgroundTile-Landscape.png"];
		}
		background.position = ccp(size.width/2, size.height/2);
        
        // add the label as a child to this Layer
		[self addChild: background];
        
        //タッチアクションを有効化する
        [self setTouchEnabled:YES];
        
        [self spriteMenus];
	}
	return self;
}


-(void)popUpClose{
    CCLOG(@"loglog");
}

-(void)spriteMenus{
    CCMenuItemSprite *menu01 = [CCMenuItemImage itemWithNormalImage:@"images/btn_start.png" selectedImage:@"images/btn_start.png" target:self selector:@selector(transition_game:)];
    
    CCMenuItemImage *menu02 = [CCMenuItemImage itemWithNormalImage:@"images/btn_store.png" selectedImage:@"images/btn_store.png" target:self selector:@selector(transition_store:)];
    /*
    CCMenuItemImage *menu03 = [CCMenuItemImage itemWithNormalImage:@"images/btn_twitter.png" selectedImage:@"images/btn_twitter.png" target:self selector:@selector(transition_twitter:)];
    
    CCMenuItemImage *menu04 = [CCMenuItemImage itemWithNormalImage:@"images/btn_facebook.png" selectedImage:@"images/btn_facebook.png" target:self selector:@selector(transition_facebook:)];
    */
    CCMenu *menu = [CCMenu menuWithItems : menu01 , menu02 , nil];
    
    menu01.rotation = appRotaion;
    menu02.rotation = appRotaion;
    //menu03.rotation = appRotaion;
    //menu04.rotation = appRotaion;
    
    [menu alignItemsVertically];
    
    [self addChild:menu];
    
}

-(void)transition_game:(CCMenuItem *)menuItem{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.3 scene:[SelectLayer scene] ]];
}

-(void)transition_store:(CCMenuItem *)menuItem{
   [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.3 scene:[StoreLayer scene] ]];
    //PFProductTableViewController
    /*
    [PFPurchase buyProduct:@"3jOJlCWa8Q" block:^(NSError *error) {
        if (!error) {
            // Run UI logic that informs user the product has been purchased, such as displaying an alert view.
            NSLog(@"purchase succeed");
        }
        else {
            NSLog(@"purchase failed : %@",error);
        }
    }];
     */
}

-(void)transition_twitter:(CCMenuItem *)menuItem{
    
}

-(void)transition_facebook:(CCMenuItem *)menuItem{
    
}

-(void) registerWithTouchDispatcher
{
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
}
-(void) ccTouchCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
}

-(void) onEnter
{
	[super onEnter];
	//[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[HelloWorldLayer scene] ]];
}
@end