//
//  ConfigLayer.h
//  orientationPuzzle
//
//  Created by takumi ninomiya on 2012/11/22.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Config.h"

@interface ConfigLayer : CCLayer{}

+(CCScene *)scene;

@end
