//
//  AppDelegate.h
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/09.
//  Copyright takumi ninomiya 2012年. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import "GADBannerView.h"

@interface MyNavigationController : UINavigationController <CCDirectorDelegate>
@end

@interface AppController : NSObject <UIApplicationDelegate,GADBannerViewDelegate>
{
	UIWindow *window_;
	MyNavigationController *navController_;
    
	CCDirectorIOS	*director_;							// weak ref
    
    GADBannerView *adBanner_;
}

@property (nonatomic, retain) UIWindow *window;
@property (readonly) MyNavigationController *navController;
@property (readonly) CCDirectorIOS *director;

@end
