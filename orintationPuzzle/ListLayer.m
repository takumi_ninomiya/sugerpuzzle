//
//  ListLayer.m
//  sugarPuzzle
//
//  Created by takumi on 2013/01/15.
//  Copyright 2013年 takumi ninomiya. All rights reserved.
//

#import "Config.h"
#import "ListLayer.h"
#import "PictureLayer.h"


@implementation ListLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	ListLayer *layer = [ListLayer node];
    
	[scene addChild: layer];
	
	return scene;
}

//
-(id) init
{
	if( (self=[super init])) {
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(
                                                             NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSLog(@"%@", paths[0]);

        
        CCMenu *menu = [CCMenu node];
        menu.position = ccp(0,0);
        NSString *dir = @"images/book";
        NSString *type = @".png";
        int num = 4;
        
        int $width = size.width / 5;
        int $height = size.height / 4.8;
        
        for (int i = 0; i < num; i++){
            if(i > 1){
                $height = -1 * size.height / 4.8;
            }
            
            if(i % 2 == 1){
                $width = size.width / 5;
            }else{
                $width = -1 * size.width / 5;
            }
            
            NSString *id_num = [NSString stringWithFormat:@"%@%d%@",dir,(i + 1),type];
            CCMenuItemImage *series = [CCMenuItemImage itemWithNormalImage:id_num selectedImage:id_num target:self selector:@selector(touchSeries:)];
            series.position = ccp(size.width/2 + $width , size.height/2 + $height);
            series.scale = 0.25f;
            
            [menu addChild:series z:i tag:i+1];
        }
        
        [self addChild:menu];
        //タッチアクションを有効化する
        [self setTouchEnabled:YES];
        
	}
	return self;
}

-(void)touchSeries:(CCMenuItem *)item{
    CCLOG(@"item = %d",item.tag);
    selectSeries = item.tag;
    [[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:0.2 scene:[PictureLayer scene] ]];
}


-(void) registerWithTouchDispatcher
{
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}


-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
}

-(void) ccTouchCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}

-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
}

@end
