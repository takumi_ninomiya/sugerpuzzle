//
//  TopScene.m
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/09.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import "TopLayer.h"
#import "MenuLayer.h"
#import "GameLayer.h"
#import "NewsLayer.h"
#import "Config.h"
#import <Parse/Parse.h>


@implementation TopLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	
    TopLayer *layer = [TopLayer node];
    [scene addChild:layer];
	
    return scene;
}

//
-(id) init
{
	if( (self=[super init])) {
		CGSize size = [[CCDirector sharedDirector] winSize];
        
		CCSprite *background;
		
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
			background = [CCSprite spriteWithFile:@"images/backgroundTile.png"];
		} else {
			background = [CCSprite spriteWithFile:@"images/backgroundTile-Landscape.png"];
		}
		background.position = ccp(size.width/2, size.height/2);
        
        // add the label as a child to this Layer
		[self addChild: background];
        
        //タッチアクションを有効化する
        [self setTouchEnabled:YES];
        
        [self spriteButton];
        
        if(isAdBannerVisible == YES){
            [self addBannerView];
        }
	}
	return self;
}

-(void)addBannerView{
    
    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
    [adBanner_ setRootViewController:[app navController]];
    
    adBanner_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    adBanner_.adUnitID = ADUNIT_ID;
    adBanner_.delegate = self;
    //adBanner_.rootViewController = self;
    GADRequest *req = [GADRequest request];
    req.testing = YES;
    [adBanner_ loadRequest:req];
    
    //デバイスが縦の時の処理
    CGAffineTransform t1 = CGAffineTransformMakeRotation(M_PI * 90 / 180.0);
    CGAffineTransform t2 = CGAffineTransformMakeTranslation(135, 135);
    adBanner_.transform = CGAffineTransformConcat(t2, t1);
    
    [[[CCDirector sharedDirector] view] addSubview:adBanner_];
}


-(void)spriteButton{
    CCMenuItemSprite *menu01 = [CCMenuItemImage itemWithNormalImage:@"images/TapStart@2x.png" selectedImage:@"images/TapStart@2x.png" target:self selector:@selector(transition_menu:)];
    
    menu01.rotation = appRotaion;
    
    CCMenu *menu = [CCMenu menuWithItems : menu01 , nil];
    
    [menu alignItemsVertically];
    
    [self addChild:menu];
}


-(void)transition_menu:(CCMenuItem *)menuItem{
    NSLog(@"test : %@" , menuItem);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    isFirst = [defaults boolForKey:@"isFirst"];
    
    if(isFirst == YES){
        [[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:0.3 scene:[NewsLayer scene] ]];
    }else{
        [[CCDirector sharedDirector] replaceScene:[CCTransitionCrossFade transitionWithDuration:0.3 scene:[MenuLayer scene] ]];
    }
    

}


-(void) registerWithTouchDispatcher
{
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
}
-(void) ccTouchCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
}




-(void) onEnter
{
	[super onEnter];
}
@end


