//
//  SelectLayer.m
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/09.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//


#import "SelectLayer.h"
#import "GameLayer.h"
#import "CCScrollLayer.h"
#import "ListLayer.h"

@implementation SelectLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	SelectLayer *layer = [SelectLayer node];
	[scene addChild: layer];
	
	return scene;
}

//
-(id) init
{
	if( (self=[super init])) {
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
		CCSprite *background;
		if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
			background = [CCSprite spriteWithFile:@"images/backgroundTile.png"];
		} else {
			background = [CCSprite spriteWithFile:@"images/backgroundTile-Landscape.png"];
		}
		background.position = ccp(size.width/2, size.height/2);        
		[self addChild: background];
        
        
        [self setSelectImage];
        
        //タッチアクションを有効化する
        [self setTouchEnabled:YES];
	}
	return self;
}

-(void)setSelectImage{
    NSMutableArray *layerArray = [NSMutableArray array];
    for(int i = 0; i < 4; i++){
        ListLayer *block = [ListLayer node];
        block.tag = i;
        [layerArray addObject:block];
    }
    
    CCScrollLayer *scrolllayer = [[CCScrollLayer alloc] initWithLayers:layerArray widthOffset:0];
    scrolllayer.showPagesIndicator = NO;
    [self addChild:scrolllayer];
    

}


-(void) registerWithTouchDispatcher
{
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return YES;
}


-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    int imageCount = 4;
    
    CGPoint locationInView = [touch locationInView:[touch view]];//タッチイベントの位置を取得
    CGPoint location = [[CCDirector sharedDirector] convertToGL:locationInView];
    
    for (int i = 0; i < imageCount; i++){
        
        CCSprite *sprite = [sprites objectAtIndex:i];
        
        float h = sprite.contentSize.height;
        float w = sprite.contentSize.width;
        float x = sprite.position.x - (sprite.contentSize.width / 2);
        float y = sprite.position.y - (sprite.contentSize.height / 2);
        CGRect rect = CGRectMake(x, y, w, h);
        
        if(CGRectContainsPoint(rect, location)){
            NSLog(@"rect = %f",rect.size.width);
        }
        
    }
    
}
-(void) ccTouchCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
}



-(void) onEnter
{
	[super onEnter];
}

@end
