//
//  LibraryLayer.m
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/13.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import "LibraryLayer.h"

//TODO-- ライブラリから画像を読み込み

@implementation LibraryLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	LibraryLayer *layer = [LibraryLayer node];
    
	[scene addChild: layer];
	
	return scene;
}

//
-(id) init
{
	if( (self=[super init])) {
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *cameraView = [CCSprite node];
        [self addChild:cameraView];
        [self runAction:[CCFollow actionWithTarget:cameraView worldBoundary:CGRectMake(0, 0, size.width, size.height)]];
        
        cameraView.opacity = 0.0f;
        CCLOG(@"this is cameraView");
        
        
        [self pickPhoto];
        
        /*
         
         CCSprite *background;
         
         if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
         background = [CCSprite spriteWithFile:@"images/texture.png"];
         background.rotation = 90;
         } else {
         background = [CCSprite spriteWithFile:@"Default-Landscape~ipad.png"];
         }
         background.position = ccp(size.width/2, size.height/2);
         
         // add the label as a child to this Layer
         [self addChild: background];
         */
        //タッチアクションを有効化する
        [self setTouchEnabled:YES];
        
	}
	return self;
}

-(void)pickPhoto{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    //ライブラリから呼び出しの場合
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    imagePicker.wantsFullScreenLayout = YES;
    imagePicker.allowsEditing = YES;//編集の可否を設定
    
    //[self presentModalViewController:imagePicker animated:YES];
    //[[[CCDirector sharedDirector] openGLView ] addSubview:imagePicker.view];
    
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    newImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    [picker dismissModalViewControllerAnimated:YES];
    [picker.view removeFromSuperview];
    [picker release];
    
    CCSprite *imageFromPicker = [CCSprite spriteWithCGImage:newImage.CGImage key:nil];
    [self addChild:imageFromPicker];
}



-(void) registerWithTouchDispatcher
{
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
}
-(void) ccTouchCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
}


@end
