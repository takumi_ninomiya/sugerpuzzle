//
//  SelectLayer.h
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/09.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Config.h"

@interface SelectLayer : CCLayer
{
    UIScrollView *scrollView;
    CCArray *rotateIDs;     //回転認識用　配列
    CCArray *sprites;    //画像格納用　配列
}

+(CCScene *) scene;

@end
