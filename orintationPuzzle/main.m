////
//  main.m
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/09.
//  Copyright takumi ninomiya 2012年. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
    [pool release];
    return retVal;
}
