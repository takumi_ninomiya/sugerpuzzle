//
//  CreditScene.m
//  orientationPuzzle
//
//  Created by takumi ninomiya on 2012/11/22.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import "CreditLayer.h"

@implementation CreditLayer

+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
	CreditLayer *layer = [CreditLayer node];
    
	[scene addChild: layer];
	
	return scene;
}

//
-(id) init
{
	if( (self=[super init])) {
		//CGSize size = [[CCDirector sharedDirector] winSize];
        [self setTouchEnabled:YES];
        
	}
	return self;
}

-(void) registerWithTouchDispatcher
{
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
}
-(void) ccTouchCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
}

@end
