//
//  GameLayer.m
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/13.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import "GameLayer.h"
#import "SelectLayer.h"

@implementation GameLayer

int t = 0;

int hour = 0;
int minute = 0;
int sec = 0;


+(CCScene *) scene
{
	CCScene *scene = [CCScene node];
    GameLayer *layer = [GameLayer node];
	[scene addChild: layer];
	return scene;
}

//
-(id) init
{
	if((self=[super init])) {
        CGSize size = [[CCDirector sharedDirector] winSize];
        sprites = [[CCArray alloc]init];
        rotateIDs = [[CCArray alloc] init];
    
        //配列にランダムにいれる
        int col = levels;
        int row = levels;
    
        int count = col * row;
        
        for(int i = 0; i < count; i++){
            int num = [self getRandInt:0 max:3];
            [rotateIDs addObject:[NSNumber numberWithInt:num]];
        }
        
        [self setImages];
        
        hour = 0;
        minute = 0;
        sec = 0;
        
        time_Label = [CCLabelTTF labelWithString:@"time:00:00:00" fontName:@"Marker Felt" fontSize:24];
        time_Label.position = ccp(size.width - 80, 10);
        [self addChild:time_Label];
        
        CCMenuItemImage *backBtn = [CCMenuItemImage itemWithNormalImage:@"images/btn_back.png" selectedImage:@"images/btn_back.png" target:self selector:@selector(pageBack:)];
        backBtn.scale = 0.5f;
        
        CCMenu *menu = [CCMenu menuWithItems:backBtn, nil];
        menu.position = ccp(125, 14);
        [self addChild:menu];
        
        //2秒後、1秒間隔で、3回繰り返す
        [self schedule:@selector(CountDowns:)interval:1 repeat:3 delay:2];
        
        [self schedule:@selector(onTimer:)interval:1 repeat:999999 delay:6];
        
        //タッチアクションを有効化する
        [self setTouchEnabled:YES];

	}
    
	return self;
}

-(void)pageBack:(id)sender
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:2.0 scene:[SelectLayer scene] ]];
}

 
-(void)CountDowns:(ccTime)dt
{
    t ++;
    
    switch (t) {
        case 3:
                NSLog(@"count %d",4 - t);
            break;
            
        case 2:
                NSLog(@"count %d",4 - t);
            break;
            
        case 1:
                NSLog(@"count %d",4 - t);
            break;
            
            
        case 0:
            
            break;
            
        default:
            break;
    }
}

-(void)onTimer:(ccTime)dt
{
    NSString *str_hour;
    NSString *str_minute;
    NSString *str_sec;
    
    
    if(sec < _SEC_MAX){
        sec++;
    }
    
    if(sec > 59){
        minute ++;
        sec = 0;
    }
    
    if(minute > 59){
        hour ++;
        minute = 0;
    }
    
    if(hour < 10){ str_hour = [[NSString alloc] initWithFormat:@"0%d",hour]; }else{ str_hour = [[NSString alloc] initWithFormat:@"%d",hour]; }
    if(minute < 10){ str_minute = [[NSString alloc] initWithFormat:@"0%d",minute]; }else{ str_minute = [[NSString alloc] initWithFormat:@"%d",minute]; }
    if(sec < 10){ str_sec = [[NSString alloc] initWithFormat:@"0%d",sec]; }else{ str_sec = [[NSString alloc] initWithFormat:@"%d",sec]; }
    
    
    NSString *str = [[NSString alloc] initWithFormat:@"time %@:%@:%@",str_hour,str_minute,str_sec];
    [time_Label setString:[NSString stringWithFormat:@"%@",str]];
    NSLog(@"%@",str);
    
}



//数字を読み込む
-(int)getRandInt:(int)min max:(int)max{
    static int randInitFlag;
    
    if(randInitFlag ==0){
        srand(time(NULL));
        randInitFlag = 1;
    }
    
    return min + (int)(rand() * (max - min + 1.0) / (1.0 + RAND_MAX));
}

-(void)imageRotateCheck{
    int col = levels;
    int row = levels;
    
    int count = col * row;
    int result = 0;
    
    for ( int i = 0; i < count; i++){
        int target = [[rotateIDs objectAtIndex:i] intValue];
        
        if(target != 0){
            result++;
        }
    }
    
    if(result == 0){
        CCLOG(@"clear");
        [self clearMethod];
    }
}


-(void)clearMethod{
    /*
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    int col = levels;
    int row = levels;
    
    int _each = 50;
    
    int i = 0;
    //const int max = col * row;
    
    imagePath = selectImagePath;
    
    for (int y = 0; y < row; y++){
        for(int x = 0; x < col; x++){
            
            int panel_x = (_each/2) +(x * _each) + BANNER_HEIGHT + 30;
            int panel_y = size.height - (_each/2) - (y * _each) - 30;
            
            CCTexture2D *tex = [[CCTextureCache sharedTextureCache] addImage:imagePath];
            CCSprite *panel = [CCSprite spriteWithTexture:tex rect:CGRectMake(x * _each + 30, y * _each + 30, _each, _each)];
            
            //パネルの位置指定
            panel.position = ccp(panel_x , panel_y );
            
            int rotateID = [[rotateIDs objectAtIndex:i] intValue];
            panel.rotation = rotateID * 90;
            //配列へ格納
            [sprites addObject:panel];
            //画像を表示
            [self addChild:panel];
            
            CCSprite *bebel = [CCSprite spriteWithFile:@"images/bebel.png"];
            bebel.scale = 1.0f;
            bebel.position = ccp(_each / 2 , _each / 2 );
            
            [panel addChild:bebel];
            
            i++;
        }
    }
     */
}


//画像をセットする
-(void)setImages{
    
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    imagePath = selectImagePath;
    CCSprite *image = [CCSprite spriteWithFile:imagePath];
    CGRect _rect = CGRectMake(0, 0, image.contentSize.width, image.contentSize.height);
    
    int col = levels;
    int row = levels;
    int _each = _rect.size.width / levels;
    int i = 0;
    
    
    for (int y = 0; y < row; y++){
        
        for(int x = 0; x < col; x++){
        
            int panel_x = (_each / 2) +(x * _each) + BANNER_HEIGHT;
            int panel_y = size.height - (_each / 2) - (y * _each);
            
            CCTexture2D *tex = [[CCTextureCache sharedTextureCache]  addImage:imagePath];
            CCSprite *panel = [CCSprite spriteWithTexture:tex rect:CGRectMake(x * _each, y * _each, _each, _each)];
            //[panel setTextureRect:_rect];
            
            //パネルの位置指定
            panel.position = ccp(panel_x , panel_y );
            
            int rotateID = [[rotateIDs objectAtIndex:i] intValue];
            
            panel.rotation = rotateID * 90;
            //配列へ格納
            [sprites addObject:panel];
            //画像を表示
            [self addChild:panel];
            
            //id rotateAction = [CCRotateBy actionWithDuration:0.8 angle:rotateID * 90];
            //[panel runAction:rotateAction];
            
            CCSprite *bebel = [CCSprite spriteWithFile:@"images/bebel.png"];
            switch (levels) {
                case 3:
                    bebel.scale = 1.0f;
                    break;
                case 4:
                    bebel.scale = 0.8f;
                    break;
                case 5:
                    bebel.scale = 0.6f;
                    break;
                    
                default:
                    break;
            }
            
            
            bebel.position = ccp( _each / 2 , _each / 2 );
            
            [panel addChild:bebel];
            
            i++;
        }
    }
}


-(void) registerWithTouchDispatcher
{
    CCDirector *director = [CCDirector sharedDirector];
    [[director touchDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    isRotate = YES;
    
    return YES;
}


-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    
    if(isRotate == YES){
    
        int col = levels;
        int row = levels;
        int imageCount = col * row;
        
        CGPoint locationInView = [touch locationInView:[touch view]];//タッチイベントの位置を取得
        CGPoint location = [[CCDirector sharedDirector] convertToGL:locationInView];
        
        for (int i = 0; i < imageCount; i++){
            
            CCSprite *sprite = [sprites objectAtIndex:i];
            
            float h = sprite.contentSize.height;
            float w = sprite.contentSize.width;
            float x = sprite.position.x - (sprite.contentSize.width);
            float y = sprite.position.y;// - (sprite.contentSize.height );
            CGRect rect = CGRectMake(x, y, w, h);
            
            //タップされた位置にあるオブジェクトに来たら、以下の処理をして脱出
            if(CGRectContainsPoint(rect, location)){
                
                int rotateID = [[rotateIDs objectAtIndex:i] intValue];
                
                int rotateAngle = 0;
                
                rotateAngle += 90;
                
                id rotateAction = [CCRotateBy actionWithDuration:0.3 angle:rotateAngle];
                id funcAction = [CCCallFuncN actionWithTarget:self selector:@selector(rotationInt:)];
                id seqAction = [CCSequence actions:rotateAction, funcAction, nil];
                [sprite runAction:seqAction];
                
                rotateID ++;
                if(rotateID > 3){
                    rotateID = 0;
                }
                
                [rotateIDs replaceObjectAtIndex:i withObject:[NSNumber numberWithInt:rotateID]];
                
                [self imageRotateCheck];
                
                break;
            }
        }
    }
    
    
}

-(void)rotationInt:(CCSprite *)sprite{
    isRotate = NO;
    //CCLOG(@"target rotation : %f",sprite.rotation);
    //数字が直角数値以外になった場合の調整
    if(sprite.rotation > 0 && sprite.rotation <= 90){
        sprite.rotation = 90;
    }else if(sprite.rotation > 90 && sprite.rotation <= 180){
        sprite.rotation = 180;
    }else if(sprite.rotation > 180 && sprite.rotation <= 270){
        sprite.rotation = 270;
    }else{
        sprite.rotation = 0;
    }
    
}


                                                   
                                                   
-(void) ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
    isRotate = NO;
}
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    isRotate = NO;
}


@end
