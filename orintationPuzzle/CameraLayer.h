//
//  CameraLayer.h
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/09.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Config.h"

@interface CameraLayer : CCLayer<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIWindow *window;
    UIImage *newImage;
}

+ (CCScene *)scene;

@end
