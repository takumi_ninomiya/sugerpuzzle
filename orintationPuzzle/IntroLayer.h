//
//  IntroScene.h
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/09.
//  Copyright takumi ninomiya 2012年. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "Config.h"

// HelloWorldLayer
@interface IntroLayer : CCLayer{
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
