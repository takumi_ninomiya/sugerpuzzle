//
//  GameLayer.h
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/13.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Config.h"
#import "Timer.h"

@interface GameLayer : CCLayer{
    
    NSString *imagePath;    //読み込まれる画像のパス
    CCArray *rotateIDs;     //回転認識用　配列
    CCArray *sprites;    //画像格納用　配列
    BOOL isRotate;
    
    NSTimer *timer;
    NSDate *stdata;
    CCLabelTTF *time_Label;
    
    int *dt;
    
    
}

+(CCScene *)scene;

@end
