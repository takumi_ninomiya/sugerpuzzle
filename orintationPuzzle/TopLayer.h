//
//  TopScene.h
//  orintationPuzzle
//
//  Created by takumi ninomiya on 2012/11/09.
//  Copyright (c) 2012年 takumi ninomiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Config.h"
//#import "AdBannerView.h"

@interface TopLayer : CCLayer<GADBannerViewDelegate,UIApplicationDelegate>
{
    GADBannerView *adBanner_;
}
+(CCScene *)scene;

@end